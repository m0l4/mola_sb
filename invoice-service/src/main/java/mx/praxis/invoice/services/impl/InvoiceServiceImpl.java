package mx.praxis.invoice.services.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import mx.praxis.invoice.bean.Invoice;
import mx.praxis.invoice.repositories.InvoiceRepository;
import mx.praxis.invoice.services.InvoiceService;

@Service(value="invoiceServiceImpl")
public class InvoiceServiceImpl implements InvoiceService {

	InvoiceRepository invoiceRepository;
	
	public InvoiceServiceImpl(InvoiceRepository invoiceRepository) {
		this.invoiceRepository = invoiceRepository;
	}
	
	@Override
	public List<Invoice> findAllInvoices() {
		return invoiceRepository.findAll();
	}

	@Override
	public Invoice findInvoiceById(Long invoiceId) {
		return invoiceRepository.findAllByInvoiceId(invoiceId);
	}

}
