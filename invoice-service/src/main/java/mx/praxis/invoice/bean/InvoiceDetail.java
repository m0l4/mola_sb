package mx.praxis.invoice.bean;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "invoice_details")
public class InvoiceDetail implements Serializable {
	
	private static final long serialVersionUID = -6507390693396268702L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
	
	@ManyToOne
	@JoinColumn(name="invoice_id")
	private Invoice invoice;
	
	@ManyToOne
	@JoinColumn(name="product_id")
	private Product product;
	
	private Double price;
	private int quantity;
	@Column(name="total_price")
	private Double totalPrice;
	

	public InvoiceDetail() {
		super();
	}
	
	public InvoiceDetail(Invoice invoice, Product product, Double price, int quantity, Double totalPrice) {
		super();
		this.invoice = invoice;
		this.product = product;
		this.price = price;
		this.quantity = quantity;
		this.totalPrice = totalPrice;
	}
	
	
	public Invoice getInvoice() {
		return invoice;
	}
	public void setInvoice(Invoice invoice) {
		this.invoice = invoice;
	}
	public Product getProduct() {
		return product;
	}
	public void setProduct(Product product) {
		this.product = product;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public Double getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}	
}
