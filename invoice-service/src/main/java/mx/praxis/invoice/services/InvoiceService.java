package mx.praxis.invoice.services;

import java.util.List;

import mx.praxis.invoice.bean.Invoice;

public interface InvoiceService {
	List<Invoice> findAllInvoices();
	
	Invoice findInvoiceById(Long invoiceId);
}
