insert into products (product_id,name) values(1,'Product 1');
insert into products (product_id,name) values(2,'Product 2');
insert into products (product_id,name) values(3,'Product 3');
insert into products (product_id,name) values(4,'Product 4');

insert into invoices(invoice_id, date, paid, total) values(1, NOW(), 1, 5000.00);
insert into invoices(invoice_id, date, paid, total) values(2, NOW(), 0, 10000.00);

insert into invoice_details(invoice_id, product_id, price, quantity, total_price) values (1, 1, 3000, 1, 3000);
insert into invoice_details(invoice_id, product_id, price, quantity, total_price) values (1, 2, 2000, 1, 2000);

insert into invoice_details(invoice_id, product_id, price, quantity, total_price) values (2, 3, 6000, 1, 6000);
insert into invoice_details(invoice_id, product_id, price, quantity, total_price) values (2, 4, 4000, 1, 4000);


