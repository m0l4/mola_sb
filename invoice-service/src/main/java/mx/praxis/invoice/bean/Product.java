package mx.praxis.invoice.bean;


import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

@Entity
@Table(name = "products")
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
public class Product implements Serializable {

	private static final long serialVersionUID = -3494277747820266494L;

	@Id
	@Column(name="product_id")
	private Long id;

	private String name;
	
	@OneToMany(mappedBy = "product")
    Set<InvoiceDetail> items;
	
	public Product() {
		super();
	}
	
	public Product(Long id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<InvoiceDetail> getItems() {
		return items;
	}

	public void setItems(Set<InvoiceDetail> items) {
		this.items = items;
	}

}
