package mx.praxis.invoice.controller;

import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mx.praxis.invoice.bean.Invoice;
import mx.praxis.invoice.services.InvoiceService;

@RestController
@RequestMapping("/")
public class InvoiceController {
	private InvoiceService invoiceService;

	public InvoiceController(InvoiceService invoiceService) {
		this.invoiceService = invoiceService;
	}

	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Invoice>> getInvoices() {
		List<Invoice> invoices = invoiceService.findAllInvoices();
		return ResponseEntity.ok(invoices);
	}

	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Invoice> getInvoiceById(@PathVariable Long id) {
		Invoice invoice = invoiceService.findInvoiceById(id);
		return ResponseEntity.ok(invoice);
	}
}
