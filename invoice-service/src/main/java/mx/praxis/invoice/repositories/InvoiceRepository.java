package mx.praxis.invoice.repositories;

import java.util.List;

import org.springframework.data.repository.PagingAndSortingRepository;

import mx.praxis.invoice.bean.Invoice;


public interface InvoiceRepository extends PagingAndSortingRepository<Invoice, Long>{
	List<Invoice> findAll();
	Invoice findAllByInvoiceId(Long invoiceId);
}
