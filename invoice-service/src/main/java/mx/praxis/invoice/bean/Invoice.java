package mx.praxis.invoice.bean;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;


@Entity
@Table(name = "invoices")
@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="invoiceId")
public class Invoice implements Serializable {
	
	private static final long serialVersionUID = -1235664627060018840L;
	
	@Id
	@Column(name="invoice_id")
	Long invoiceId;
	@Temporal(TemporalType.DATE)
	Date date;
	Boolean paid;
	Double total;
	
	@OneToMany(mappedBy = "invoice")
	Set<InvoiceDetail> items;
	
	public Invoice() {
		super();
	}

	public Invoice(Long invoiceId, Date date, Boolean paid, Double total) {
		super();
		this.invoiceId = invoiceId;
		this.date = date;
		this.paid = paid;
		this.total = total;
	}

	public Long getInvoiceId() {
		return invoiceId;
	}

	public void setInvoiceId(Long invoiceId) {
		this.invoiceId = invoiceId;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Boolean getPaid() {
		return paid;
	}

	public void setPaid(Boolean paid) {
		this.paid = paid;
	}

	public Double getTotal() {
		return total;
	}

	public void setTotal(Double total) {
		this.total = total;
	}

	public Set<InvoiceDetail> getItems() {
		return items;
	}

	public void setItems(Set<InvoiceDetail> items) {
		this.items = items;
	}

}
